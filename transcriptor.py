#!/usr/bin/env python3
import sys
from os import path, listdir
import speech_recognition as sr

r = sr.Recognizer()

def testa_resultado_google(resgoo):
    if not isinstance(resgoo, dict):
        return False
    if 'alternative' not in resgoo:
        return False
    if not isinstance(resgoo['alternative'], list):
        return False
    if not resgoo['alternative']:
        return False
    if not isinstance(resgoo['alternative'][0], dict):
        return False
    if 'transcript' not in resgoo['alternative'][0]:
        return False
    return True


pasta = "."
caminhos = [path.join(pasta, nome) for nome in listdir(pasta)]
arquivos = [arq for arq in caminhos if path.isfile(arq)]
audios = [arq for arq in arquivos if arq.lower().endswith(".wav")]
print("Total de " + str(len(audios)) + " arquivos de audio (ingles) nesta pasta:")
print("======================================")

for audio in audios:
    texto = ""
    nome_arquivo = audio.replace('./', '').replace('.wav', '')
    print("Transcrevendo audio " + nome_arquivo + ".wav")

    audio = path.join(path.dirname(path.realpath(__file__)), nome_arquivo + ".wav")
    with sr.AudioFile(audio) as source:
        audio = r.record(source)
    try:
        resultado = r.recognize_google(audio, language='en-US', show_all=True)
        if not testa_resultado_google(resultado):
            raise Exception("Erro no google. Tente mais tarde..")
        texto = resultado['alternative'][0]['transcript']
    except sr.UnknownValueError:
        print("Servico Google Speech Recognition nao pode entender o audio")
    except sr.RequestError as e:
        print("Servico Google Speech Recognition nao esta disponivel; {0}".format(e))

    with open(nome_arquivo + '.txt', 'wb') as arquivo:
        arquivo.write(texto.encode('utf-8'))
